#!/usr/bin/python3

import logging      # Used for outputs
import math         # Used for ceil
import operator     # Used for xor
import os           # Used for urandom and file path manipulation
import socket       # Used for client's connection
import socketserver # Used for server's connection

from binascii import hexlify
from base64   import b64encode, b64decode # Used for encoding binary data
from hashlib  import sha1                 # Used for computing sha1 digest

# Libraries used for verifying server certificate
import OpenSSL.crypto as crypto
import cryptography.hazmat.primitives.hashes as hashes
import cryptography.hazmat.primitives.asymmetric.padding as padding

# Logging settings
logger = logging.getLogger(__name__)

def enable_debug_mode():
    logging.root.setLevel(logging.DEBUG)

    import signal
    def breakpoint(*args):
        import pdb
        pdb.set_trace()
    signal.signal(signal.SIGUSR1, breakpoint)

logging.basicConfig(
    format='\033[1m%(asctime)s %(levelname)s %(filename)s:%(lineno)d: '
        '%(message)s\033[0m', level=logging.INFO)

# Exceptions
class UnexpectedMessageType(Exception):
    pass

class UnexpectedMessageParameters(Exception):
    pass

class ConnectionClosed(Exception):
    pass

class CorruptedEncryption(Exception):
    pass

# Base class for connection endpoints on both server and client
class SecureConnection:
    # Separator used between fields of initial clear-text messages
    SEPARATOR = b','

    def __init__(self, rfile, wfile):
        self.rfile              = rfile
        self.wfile              = wfile
        self.session_key_length = 8
        self.max_session_key    = 2 ** (8 ** self.session_key_length)
        self.padding            = padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA1()),
            algorithm=hashes.SHA1(), label=None)
        self.block_length = 20
        self.buffer       = b''
        self.blocks_read    = 0
        self.blocks_written = 0

    def setup(self):
        raise NotImplemented('This method should be overriden.')

    # Self explanatory
    def send_clear_data(self, data):
        data = self.SEPARATOR.join(data) + b'\n'
        self.wfile.write(data)
        self.wfile.flush()

        logger.debug('CLEAR DATA SENT: %s', str(data, 'ascii'))

    # Self explanatory
    def receive_clear_data(self, expected = None, param_count = None):
        data = self.rfile.readline()
        logger.debug('CLEAR DATA RECEIVED: %s', str(data, 'ascii'))

        if data == '':
            raise ConnectionClosed()
        data = data[:-1].split(self.SEPARATOR)
        if expected != None and data[0] != expected:
            raise UnexpectedMessageType()
        if param_count != None and len(data) != param_count:
            raise UnexpectedMessageParameters()
        return data

    # Low-level function used for sending data as encrypted blocks
    def send_encrypted_data(self, data):
        data = self.buffer + data
        i = -1
        for i in range(len(data) // self.block_length):
            self.blocks_written += 1
            if self.blocks_written % 100000 == 0:
                logger.info('Blocks written: %d/%d', self.blocks_written,
                    len(data) // self.block_length)
            block_start = i * self.block_length
            block = data[block_start:block_start+self.block_length]
            logger.debug('SENDING[%d] (~E): %s', self.blocks_written,
                hexlify(block))
            block = self.encrypt_block(block)
            logger.debug('SENDING[%d] (E): %s', self.blocks_written,
                hexlify(block))
            self.wfile.write(block)
        self.wfile.flush()

        block_start = (i+1) * self.block_length
        self.buffer = data[block_start:]

    # Low-level function used for receiving data as encrypted blocks
    def receive_encrypted_data(self, block_count = 1):
        decrypted_blocks = bytearray()
        while block_count > 0:
            self.blocks_read += 1
            if self.blocks_read % 100000 == 0:
                logger.info('Blocks read: %d', self.blocks_read)
            block = self.rfile.read(self.block_length)
            logger.debug('RECEIVED[%d] (~D): %s', self.blocks_read,
                hexlify(block))
            if not block:
                raise ConnectionClosed()
            if len(block) != self.block_length:
                raise ValueError('Block length is not correct.')

            block = self.decrypt_block(block)
            logger.debug('RECEIVED[%d] (D): %s', self.blocks_read, hexlify(block))
            decrypted_blocks.extend(block)
            block_count -= 1
        return bytes(decrypted_blocks)

    # Low-level function used for padding and sending remaining buffer
    def flush_encrypted_data(self):
        if len(self.buffer) > 0:
            self.blocks_written += 1
            self.buffer += b'\xFF' * (self.block_length - len(self.buffer))
            logger.debug('FLUSHING[%d] (~E): %s', self.blocks_written,
                hexlify(self.buffer))
            block = self.encrypt_block(self.buffer)
            logger.debug('FLUSHING[%d] (E): %s', self.blocks_written,
                hexlify(block))
            self.wfile.write(block)
            self.wfile.flush()
            self.buffer = b''

    # Low-level function used for encrypting a single block
    def encrypt_block(self, data):
        assert(len(data) == self.block_length)
        self.local_enc_seed = self.increase_key(self.local_enc_seed, 2)
        encryption_key = sha1(
            self.local_enc_seed + self.local_enc_feedback).digest()
        encrypted = bytes(map(operator.xor, data, encryption_key))
        self.local_enc_feedback = encrypted
        return encrypted

    # Low-level function used for decrypting a single block
    def decrypt_block(self, data):
        assert(len(data) == self.block_length)
        self.remote_enc_seed = self.increase_key(self.remote_enc_seed, 2)
        decryption_key = sha1(
            self.remote_enc_seed + self.remote_enc_feedback).digest()
        decrypted = bytes(map(operator.xor, data, decryption_key))
        self.remote_enc_feedback = data
        return decrypted

    # Low-level function used for encrypting a message with rsa key
    def encrypt_rsa(self, key, msg):
        return key.encrypt(msg, self.padding)

    # Low-level function used for decrypting a message with rsa key
    def decrypt_rsa(self, key, msg):
        return key.decrypt(msg, self.padding)

    # Low-level function used for increasing bytes key
    def increase_key(self, key, amount):
        key = int.from_bytes(key, 'big')
        return ((key + amount) % self.max_session_key).to_bytes(
            self.session_key_length, 'big')

    # High-level function used for encrypting and sending data with integrity
    # check
    def send_encrypted_msg(self, payload):
        payload_len      = len(payload).to_bytes(4, 'big')
        payload_len_hash = sha1(payload_len).digest()
        payload_hash     = sha1(payload).digest()
        self.send_encrypted_data(payload_len)
        self.send_encrypted_data(payload_len_hash)
        self.send_encrypted_data(payload_hash)
        self.send_encrypted_data(payload)
        self.flush_encrypted_data()
        logger.info('Encrypted payload has been sent.\n'
            ' Payload Length: %d bytes\n'
            ' Payload Length Hash: %s\n'
            ' Payload Hash: %s\n',
            len(payload), hexlify(payload_len_hash), hexlify(payload_hash))

    # High-level function used for receiving encrypted data, decrypting it and
    # checking its integrity
    def receive_encrypted_msg(self):
        buf              = self.receive_encrypted_data(3)
        payload_len_bin  = buf[:4]
        payload_len_hash = buf[4:24]
        computed_hash    = sha1(payload_len_bin).digest()

        # Integrity of payload length checked here
        if computed_hash != payload_len_hash:
            raise CorruptedEncryption()

        payload_len  = int.from_bytes(payload_len_bin, 'big')
        payload_hash = buf[24:44]

        logger.info(
            'Integrity of payload length has been checked.\n'
            ' Payload Length: %d bytes\n'
            ' Payload Length Hash:\n  Message: %s\n  Computed: %s',
            payload_len, hexlify(payload_len_hash), hexlify(computed_hash))

        if payload_len <= 16:
            payload = buf[44:44+payload_len]
        else:
            remaining_block_count = math.ceil(
                (payload_len - 16) / self.block_length)
            buf = buf[44:] + self.receive_encrypted_data(remaining_block_count)
            payload = buf[:payload_len]

        computed_hash = sha1(payload).digest()

        # Integrity of payload checked here
        if computed_hash != payload_hash:
            raise CorruptedEncryption()

        logger.info(
            'Integrity of payload has been checked.\n'
            ' Payload Hash:\n  Message: %s\n  Computed: %s',
            hexlify(payload_hash), hexlify(computed_hash))

        return payload


# Client side of secure connection
class ClientSecureConnection(SecureConnection):
    def __init__(self, rfile, wfile):
        super().__init__(rfile, wfile)

    def setup(self, client):
        # Read server certicate message
        server_cert_b64 = self.receive_clear_data(b'SERVER_CERTIFICATE', 2)[1]
        server_cert_pem = b64decode(server_cert_b64)
        server_cert = crypto.load_certificate(
            crypto.FILETYPE_PEM, server_cert_pem)

        # Verify server certificate using CA certificate
        store = crypto.X509Store()
        store.add_cert(client.ca_cert)
        store_ctx = crypto.X509StoreContext(store, server_cert)
        store_ctx.verify_certificate()
        logger.info('Server certificate is verified.')

        # Pick random session key such that it's even
        while True:
            session_key = os.urandom(self.session_key_length)
            if int.from_bytes(session_key, 'big') % 2 == 0:
                break

        # Encrypt and send session key
        enc_session_key = self.encrypt_rsa(
            server_cert.get_pubkey().to_cryptography_key(), session_key)
        self.send_clear_data([b'SESSION_KEY', b64encode(enc_session_key)])

        # Compute CES and SES
        client_enc_seed = self.increase_key(session_key, 1)
        server_enc_seed = self.increase_key(session_key, 2)

        # Set local and remote encryption keys
        self.local_enc_seed  = client_enc_seed
        self.remote_enc_seed = server_enc_seed

        # Compute IV
        self.iv = sha1(client_enc_seed + server_enc_seed)

        # Set feedbacks to IV initially (for CBC)
        self.local_enc_feedback  = self.iv.digest()
        self.remote_enc_feedback = self.iv.digest()

        logger.info('SESSION KEY: %s' % hexlify(session_key))
        logger.info('IV: %s' % self.iv.hexdigest())

# Server side of secure connection
class ServerSecureConnection(SecureConnection):
    def __init__(self, rfile, wfile):
        super().__init__(rfile, wfile)

    def setup(self, server):
        # Send server certicate message
        server_cert_pem = crypto.dump_certificate(
            crypto.FILETYPE_PEM, server.server_cert)
        self.send_clear_data(
            [b'SERVER_CERTIFICATE', b64encode(server_cert_pem)])

        # Receive and verify session key
        enc_session_key_b64 = self.receive_clear_data(b'SESSION_KEY', 2)[1]
        enc_session_key     = b64decode(enc_session_key_b64)
        session_key = self.decrypt_rsa(
            server.private_key.to_cryptography_key(), enc_session_key)

        # Compute CES and SES
        client_enc_seed = self.increase_key(session_key, 1)
        server_enc_seed = self.increase_key(session_key, 2)

        # Set local and remote encryption keys
        self.local_enc_seed  = server_enc_seed
        self.remote_enc_seed = client_enc_seed

        # Set IV
        self.iv = sha1(client_enc_seed + server_enc_seed)

        # Set feedbacks to IV initially (for CBC)
        self.local_enc_feedback  = self.iv.digest()
        self.remote_enc_feedback = self.iv.digest()

        logger.info('SESSION KEY: %s' % hexlify(session_key))
        logger.info('IV: %s' % self.iv.hexdigest())


# Client's main class
class Client:
    def __init__(self, server_hostname, server_port, ca_cert_path):
        # Set hostname and port of the server
        self.server_address = (server_hostname, server_port)

        # Read CA certificate
        with open(ca_cert_path, 'r') as f:
            self.ca_cert = crypto.load_certificate(
               crypto.FILETYPE_PEM, f.read())

    def send_command(self, filename, command):
        assert(type(command) == bytes)

        # Open connection to server
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect(self.server_address)
            with s.makefile('rb') as rfile:
                with s.makefile('wb') as wfile:
                    # Create a secure connection overlay over the socket
                    conn = ClientSecureConnection(rfile, wfile)
                    conn.setup(self)

                    # Send command
                    conn.send_encrypted_msg(command)

                    if command == b'UPLOAD':
                        # Set payload as filename and file contents
                        with open(filename, 'rb') as f:
                            payload = (
                                bytes(filename, 'ascii') + b'\x00' + f.read())

                        # Send payload over encrypted channel
                        conn.send_encrypted_msg(payload)
                    elif command == b'DOWNLOAD':
                        # Send filename to download
                        conn.send_encrypted_msg(bytes(filename, 'ascii'))

                        # Receive payload
                        data = conn.receive_encrypted_msg()

                        # Create a directory named uploaded if it doesn't exist
                        try:
                            os.mkdir('downloaded')
                        except FileExistsError:
                            pass

                        # Create file in this directory and write its contents
                        with open(
                            os.path.join('downloaded', filename), 'wb') as f:
                            f.write(data)
                            print('File written: %s (%d bytes)' % (
                                f.name, len(data)))

                    # Succeeded
                    if conn.receive_encrypted_msg() == b'SUCCESS':
                        return True

                    # Failed
                    return False

# Server's class to handle client
class ServerHandler(socketserver.StreamRequestHandler):
    def handle(self):
        # Create a secure connection overlay over the socket
        conn = ServerSecureConnection(self.rfile, self.wfile)
        conn.setup(self.server)
        try:
            # Receive command
            msg = conn.receive_encrypted_msg()
            if msg == b'UPLOAD':
                # Read filename and contents
                filename, data = conn.receive_encrypted_msg().split(b'\x00', 1)

                # Create a directory named uploaded if it doesn't exist
                try:
                    os.mkdir('uploaded')
                except FileExistsError:
                    pass

                # Create file in this directory and write its contents
                with open(
                    os.path.join('uploaded', str(filename, 'ascii')), 'wb') as f:
                    f.write(data)
                    print('File written: %s (%d bytes)' % (f.name, len(data)))
            elif msg == b'DOWNLOAD':
                # Read filename
                filename = conn.receive_encrypted_msg()

                # Set payload as filename and file contents
                with open(filename, 'rb') as f:
                    payload = f.read()

                # Send payload over encrypted channel
                conn.send_encrypted_msg(payload)
            else:
                raise ValueError('Unknown command: %s' % msg)
        except Exception:
            # In case of any failure
            conn.send_encrypted_msg(b'FAILURE')
            raise

        # In case of success
        conn.send_encrypted_msg(b'SUCCESS')


# Server's main class
class Server(socketserver.ThreadingTCPServer):
    def __init__(self, server_port, private_key_path, server_cert_path):
        # Read server certificate
        with open(server_cert_path, 'r') as f:
            self.server_cert = crypto.load_certificate(
               crypto.FILETYPE_PEM, f.read())

        # Read server's private key
        with open(private_key_path, 'r') as f:
            self.private_key = crypto.load_privatekey(
               crypto.FILETYPE_PEM, f.read())

        # Set SO_REUSEADDR and call constructor for super class.
        self.allow_reuse_address = True
        super().__init__(("0.0.0.0", server_port), ServerHandler)

