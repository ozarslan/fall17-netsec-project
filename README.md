# CS6349 Programming Project
In this project, we implement a server/client model where a client authenticates a server, and uploads/downloads file securely ensuring confidentiality and integrity.

## Requirements
* [Python](https://www.python.org/) (>= 3.4.1)
    * [virtualenv](https://pypi.python.org/pypi/virtualenv) (>= 15.1.0)
    * [pyOpenSSL](https://pypi.python.org/pypi/pyOpenSSL) (>= 17.5.0)

## Installation
```
pip install --no-cache-dir -U virtualenv
git clone git@gitlab.com:fall17-cs6349/project.git
cd project
python3 -m virtualenv .
source bin/activate
pip install --no-cache-dir -U pyOpenSSL
```

## Usage
First you need to start a server:
```
python3 server.py <server-port> <path-to-private-key-file> <path-to-server-certificate-file>
```

Then run a client:
```
python3 client.py <server-hostname> <server-port> <path-to-ca-certificate-file>
```

Type the filename and whether you would like to upload or download when client prompts. Add `-d` to enable debug mode.

Press Ctrl+C to exit.

## Protocol Description
### Authentication
1. Client creates 64-bit random value, *session key*, `SK`, which is guaranteed to be even.
2. Client connects to server.
3. Server sends its certificate.
4. Client checks if server's certificate is valid using CA's public key.
5. Client infers *public key of the server*, `PK` from the certificate.
6. Client encrypts `SK` with `PK`, and sends to server.
    * Since we use server's public key to encrypt `SK`, only corresponding private key can decrypt this message.
    * An eavesdropper cannot decrypt this message without server's private key.
7. Server decrypts message using its private key.
8. Server and client shares `SK`.

* If any of this steps fail, connection is closed.

### Encryption
1. `SK+1` is *client's encryption seed*, `CES`, which is guaranteed to be odd.
2. `SK+2` is *server's encryption seed*, `SES`, which is guaranteed to be even.
3. `LEN(payload)` is a 32-bit value, *the length of message payload*.
4. Message body `M` consists of `LEN(payload), sha1(LEN(payload)), sha1(payload), payload`.
5. `sha1(CES & SES)` is *client's initialization vector*, `CIV`.
6. `sha1(CES & SES)` is *server's initialization vector*, `SIV`.
7. For block-1, *client's encryption key*, `CEK(1)`, is `sha1((SK+1+2*i) | (CIV))`.
8. For block-1, *server's encryption key*, `SEK(1)`, is `sha1((SK+2+2*i) | (SIV))`.
7. For block i (i=2,3...n), *client's encryption key*, `CEK(i)`, is `sha1((SK+1+2*i) | C(i-1))`.
9. For block i (i=2,3,..n), *server's encryption key*, `SEK(i)`, is `sha1((SK+2+2*i) | C(i-1))`.
    * To prevent a replay attack, we used different encryption keys for each block for both client and server.
10.Then, we use CBC to transmit message body:

![IV](images/IV.png)
![Encryption](images/encryption.png)

* Since `SK` is known only by client and server, an eavesdroppper cannot determine content of the messages.
* Client and server uses different encrpytion keys so that an attacker doesn't replay server's messages back to the client, or vice versa.
* Since `SK` is random 64-bit value, it is unlikely to use it again. Therefore, this prevents an attacker replaying client's or server's messages to server or client in another session using same `SK`.

### Decryption and Integrity Check
1. Client uses `SIV` and `SEK(i)` to decrypt messages received from the server.
2. Server uses `CIV` and `CEK(i)` to decrypt messages received from the client.
3. Client/Server checks the length of message and hash of the length of message received in the beginning of the message body.
    * If they don't match, that means packets are modified, therefore connection is aborted.
4. Client/server computes sha1 of payload, and compares it to the hash of payload included in the message body.
    * If they don't match, that means packets are modified, therefore connection is aborted.

![Decryption](images/decryption.png)

* Integrity is ensured by checking hash of payload.
* If length is modified, server and client will notice it before blocking while waiting for a payload since they check the hash of the length of the message before receiving payload.

## Challenges Faced
We didn't have any major challenges. We had to make code compatible with Python 3.4.1 to be able to test it on department machines as they don't support Python 3.5. We didn't have sudo access on those machines as well, so we used virtualenv to install packages needed.

## Contributors

Omer Ozarslan
<<omer@utdallas.edu>>

Arun Prakash Themothy Prabu Vincent
<<axt161330@utdallas.edu>>
