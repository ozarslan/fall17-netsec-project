#!/usr/bin/python3

import sys
import netsec

def print_usage():
    print('usage:', sys.argv[0], '<server-port> <path-to-private-key-file> '
        '<path-to-server-certificate-file> [-d]')

if __name__ == '__main__':
    if len(sys.argv) not in (4, 5):
        print_usage()
        sys.exit(1)

    try:
        if sys.argv[4] == '-d':
            netsec.enable_debug_mode()
        else:
            print_usage()
            sys.exit(1)
    except IndexError:
        pass

    server_port, private_key_path, server_certificate_path = sys.argv[1:4]
    server = netsec.Server(
                int(server_port), private_key_path, server_certificate_path)

    print('Press Ctrl-C to stop server.')

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        print('Exiting...')

