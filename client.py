#!/usr/bin/python3

import sys
import netsec

def print_usage():
    print('usage:', sys.argv[0], ' <server-hostname> <server-port> '
          '<path-to-ca-certificate-file> [-d]')

if __name__ == '__main__':
    if len(sys.argv) not in (4, 5):
        print_usage()
        sys.exit(1)

    try:
        if sys.argv[4] == '-d':
            netsec.enable_debug_mode()
        else:
            print_usage()
            sys.exit(1)
    except IndexError:
        pass

    server_hostname, server_port, ca_cert_path = sys.argv[1:4]
    client = netsec.Client(server_hostname, int(server_port), ca_cert_path)

    print('Press Ctrl-C to exit client.')

    try:
        while True:
            filename = input('Enter filename: ')
            cmd = input('Upload/Download? ')

            repeat = True
            while repeat:
                print('Processing command...',)
                cmd = cmd.upper()
                if cmd == 'U' and client.send_command(filename, b'UPLOAD'):
                    repeat = False
                    print('Successfully uploaded.')
                elif cmd == 'D' and client.send_command(filename, b'DOWNLOAD'):
                    repeat = False
                    print('Successfully downloaded.')
                else:
                    print('Failed!')
                    answered = False
                    while not answered:
                        answer = input('Try again? (y/n) ').lower()
                        if answer == 'n':
                            repeat = False
                            answered = True
                        elif answer == 'y':
                            repeat = True
                            answered = True
    except KeyboardInterrupt:
        print('Exiting...')

